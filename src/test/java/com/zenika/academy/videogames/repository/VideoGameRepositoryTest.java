package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Profile({"test"})
public class VideoGameRepositoryTest {

    @Autowired
    VideoGamesRepository videoGamesRepository;

    @Test
    void saveAndGetTest(){
        //mocker ce qui récupère les jeux vidéo ?
        // crée un genre de jeu
        List<Genre> action = (List<Genre>) new Genre("action");;
        //crée un jeu
        VideoGame videoGame = new VideoGame();
        videoGame.setName("jeu1");
        videoGame.setGenres( action);
        //le sauvegarder
        videoGamesRepository.save(videoGame);
        //vérifier si le jeu existe
        assertEquals("", videoGamesRepository.get(videoGame.getId()));

    }

    //   @Test
    void getAllTest(){
        //crée deux jeux vidéos
        VideoGame videoGame = new VideoGame();
       // VideoGame videoGame2 = new VideoGame(1, "jeu1", new List<Genre>("Action")  );

        //sauvegarder les jeux vidéo
       videoGamesRepository.save(videoGame);
       // videoGamesRepository.save(videoGame2);

        //vérifier si ils existent


    }
}
