package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

@SpringBootTest
@Profile({"test"})
public class VideoGamesServicesTest {

    @Autowired
    VideoGamesRepository videoGamesRepository;

    @Autowired
    VideoGamesService videoGamesService;

    @Test
    void ownedVideoGamesTest() {
        //mocker le repo pour crée une liste de jeux vidéo

      //vérifier si les jeux sont bien présents


    }

    @Test
    void getOneVideoGameTest(){

        //mocker le repo pour avoir un jeu vidéo

        //vérfier si on le récupère bien

    }

    @Test
    void addVideoGameTest(){
        RawgDatabaseClient rawgDatabaseClientMock = Mockito.mock(RawgDatabaseClient.class);
       // VideoGame videoGameMock = Mockito.mock(VideoGame);

        Mockito.when(rawgDatabaseClientMock.getVideoGameFromName("test")).thenReturn(new VideoGame());

        //RawgDatabaseClient client = new RawgDatabaseClient();
     //   VideoGame newGame = client.getVideoGameFromName();


    }

}
