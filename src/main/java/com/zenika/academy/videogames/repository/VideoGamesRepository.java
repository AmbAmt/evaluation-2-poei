package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll() {
        return List.copyOf(this.videoGamesById.values());
    }

    public Optional<VideoGame> get(Long id) {
        return Optional.ofNullable(this.videoGamesById.get(id));
    }

    public void save(VideoGame v) {
        this.videoGamesById.put(v.getId(), v);
    }

    public boolean delete(Long id) {
        videoGamesById.remove(id);
        return true;
    }

    public VideoGame modifyFinished(Long id, Boolean videoGameStatus) {
        VideoGame videoGame = videoGamesById.get(id);
        videoGame.setFinished(videoGameStatus);
        return videoGame;
    }

    public Optional<VideoGame> getAllByGenre(String genre){
        //problème : retourne null
        return this.videoGamesById.values().stream().filter(v->v.getGenres().contains(genre)).findAny();
    }
}
