package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.UnexistingGameException;
import com.zenika.academy.videogames.service.UnexistingGenreException;
import com.zenika.academy.videogames.service.VideoGamesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {

    private VideoGamesService videoGamesService;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService) {
        this.videoGamesService = videoGamesService;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     *
     * Exemple :
     *
     * GET /video-games
     */
    @GetMapping
    public List<VideoGame> listOwnedVideoGames() {
        return videoGamesService.ownedVideoGames();
    }

    /**
     * Récupérer un jeu vidéo par son ID
     *
     * Exemple :
     *
     * GET /video-games/3561
     */
    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) throws UnexistingGameException {
        VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id).orElseThrow(()->new UnexistingGameException());
        if(foundVideoGame != null) {
            return ResponseEntity.ok(foundVideoGame);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     *
     * Exemple :
     *
     * POST /video-games
     * Content-Type: application/json
     *
     * {
     *     "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public VideoGame addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) throws UnexistingGameException {
        return this.videoGamesService.addVideoGame(videoGameName.getName());
    }

    /**
     * Permet de supprimer un jeu a condition que l'id de celui-ci existe
     * @param id du jeu a supprimer
     * @throws UnexistingGameException
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<Object> deleteVideoGame(@PathVariable("id") Long id){
        this.videoGamesService.removeVideoGame(id);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    /**
     * permet de motifier le status d'un jeu comme étant fini ou non
     * @param videoGameFinished modifie le status du jeu : true le jeu est terminé, fasle le jeu n'est pas terminé
     * @return
     */
    @PutMapping("/{id}")
    @ResponseStatus(value =HttpStatus.OK )
    public ResponseEntity<VideoGame> putStatusGame(@PathVariable("id") Long id, @RequestBody VideoGameNameRepresentation videoGameFinished) throws UnexistingGameException {
        this.videoGamesService.modifyFinished(id, videoGameFinished.getFinished());
        VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id).orElseThrow(()->new UnexistingGameException());
        if(foundVideoGame != null) {
            return ResponseEntity.ok(foundVideoGame);
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * permet de récupérer tous les jeux selon le genre donnée
     * @param genre filtre les jeux retournés
     * @return la liste des jeux demandé selon le genre
     * @throws UnexistingGenreException
     */
    @GetMapping("/genre")
    @ResponseStatus(value=HttpStatus.OK)
    public Optional<VideoGame> getGenre(@RequestParam(value="genre")String genre) throws UnexistingGenreException {
        return this.videoGamesService.ownedVideoGamesGenre(genre);
    }
}
