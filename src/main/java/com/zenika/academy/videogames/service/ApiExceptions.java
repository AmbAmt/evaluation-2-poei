package com.zenika.academy.videogames.service;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.FileNotFoundException;

@ControllerAdvice
public class ApiExceptions {

    @ExceptionHandler(value = {UnexistingGameException.class})
    public final ResponseEntity<ApiError> gameException() throws FileNotFoundException{
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Le jeu n'existe pas");
        return new ResponseEntity<ApiError>(apiError,apiError.getStatus());
    }

    @ExceptionHandler(value = {UnexistingGenreException.class})
    public final ResponseEntity<ApiError> genreException() throws FileNotFoundException{
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, "Le genre n'existe pas");
        return new ResponseEntity<ApiError>(apiError,apiError.getStatus());
    }
}
