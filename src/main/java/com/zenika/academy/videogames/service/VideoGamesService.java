package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    private RawgDatabaseClient  client;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient rawgDatabaseClient) {
        this.videoGamesRepository = videoGamesRepository;
        this.client = rawgDatabaseClient;
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

    public Optional<VideoGame> getOneVideoGame(Long id) {
        return this.videoGamesRepository.get(id);
    }

    public VideoGame addVideoGame(String name) throws UnexistingGameException {
        VideoGame newGame = Optional.ofNullable(client.getVideoGameFromName(name)).orElseThrow(()-> new UnexistingGameException());

        videoGamesRepository.save(newGame);

        return newGame;
    }

    public boolean removeVideoGame(Long id){
        videoGamesRepository.delete(id);
        return true;
    }

    public Optional<VideoGame> modifyFinished(Long id, Boolean videoGameStatus){
        videoGamesRepository.modifyFinished(id, videoGameStatus);
         return videoGamesRepository.get(id);
    }

    public Optional<VideoGame> ownedVideoGamesGenre(String genre) throws UnexistingGenreException {
       return Optional.ofNullable(videoGamesRepository.getAllByGenre(genre)).orElseThrow(()->new UnexistingGenreException());
    }
}
